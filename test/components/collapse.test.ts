import { expect, test } from 'vitest'
import { mount } from '@vue/test-utils'
import Collapse from '../../src/components/ui/Collapse.vue'

test('mount component', async () => {
    expect(Collapse).toBeTruthy()
    const wrapper = mount(Collapse,{
        slots: {
            default: 'Collapse Content',
        }
    })

    const icon = wrapper.get('svg');
    const collapseItem = wrapper.get('div');

    expect(collapseItem.text()).toBe('Collapse Content');
    expect(collapseItem.classes()).toContain('hidden');

    await icon.trigger('click');
    expect(collapseItem.classes()).toHaveLength(0);

    await icon.trigger('click');
    expect(collapseItem.classes()).toContain('hidden');

})
