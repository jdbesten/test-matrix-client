import { defineConfig } from 'histoire'
import { HstVue } from '@histoire/plugin-vue'

export default defineConfig({
    setupFile: './src/histoire-setup.ts',
    plugins: [
        HstVue(),
    ],
    backgroundPresets: [
        {
            label: 'Light',
            color: '#fff',
            contrastColor: '#333'
        },
        {
            label: 'Dark',
            color: '#001242',
            contrastColor: '#eee'
        },
    ],
})
