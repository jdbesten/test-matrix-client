import { setActivePinia, createPinia } from 'pinia'
import { describe, beforeEach, assert, expect, test } from 'vitest'
import { useSettings } from '../../src/store/settings'

describe('Settings Store', () => {

    let settings = {} as any;

    beforeEach(() => {
        setActivePinia(createPinia())
        settings = useSettings();
        settings.pagination = 10;
        settings.visibleEventTypes = ['m.room.message'];
    })

    describe('pagination', () => {
        test('default', () => {
            expect(settings.pagination).toBe(10);
        })

        test('getPagination', () => {
            expect(settings.getPagination).toBe(10);
        })

        test('setPagination', () => {
            settings.setPagination(20);
            expect(settings.getPagination).toBe(20);
        })
    })

    describe('visibleEventTypes', () => {
        test('default', () => {
            expect(settings.getVisibleEventTypes).toHaveLength(1);
            expect(settings.getVisibleEventTypes).toEqual(['m.room.message']);
        })

        test('getVisibleEventTypes', () => {
            settings.visibleEventTypes = ['one', 'two'];
            expect(settings.getVisibleEventTypes).toHaveLength(2);
            expect(settings.getVisibleEventTypes).toEqual(['one', 'two']);
        })

    })

})
