const routes = [
    { path: '/', name: 'home', component: () => import ('./components/pages/Home.vue') },
    { path: '/login', name: 'login', component: () => import ('./components/pages/Login.vue') },
    { path: '/logout', name: 'logout', component: () => import ('./components/pages/Logout.vue') },
    { path: '/settings', name: 'settings', component: () => import ('./components/pages/Settings.vue') },
    { path: '/room/:id', name: 'room', component: () => import ('./components/pages/Room.vue') },
    { path: '/nop', name: 'nop', component: () => import ('./components/pages/NotImplemented.vue') },
];
export {routes};
