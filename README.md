# Pubhubs Client

A [PubHubs](https://pubhubs.net/) client for a better user experience with the PubHubs network as Matrix Element could give us.

Build on top of `matrix-js-sdk` with `TypeScript`, `VueJs3` and `Tailwind`.

# Installing

For normal use this should automically be done with the PubHubs deployment and install procedure.


# Development

[Node](https://nodejs.org) should be present on you're system.

```
npm install
```

Set url of the hubs-homeserver in `.env`:

```
VUE_APP_BASEURL = https://....
```


## Theming components

Theming can be changed in: `./src/assets/pubhubs-theme.js`.

Show and test the components (we use [histoire](https://histoire.dev)):

```
npm run story:dev
```

Icons (svg) are all in one TypeScript object (strings) in `./src/assets/icons.ts`

## Development

```
npm run serve
```

## Production

```
npm run build
```

As a PWA:

```
npm run pwa
```

## Testing

```
npm run test
```

## Lints and fixes files
```
npm run lint
```
