import { assert, expect, test } from 'vitest'
import {routes} from '../src/routes.js'

test('routes', () => {
    expect(routes).toBeTypeOf('object');
    expect(Object.keys(routes).length).toBe(6);
})
