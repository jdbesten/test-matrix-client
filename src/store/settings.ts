/**
 * This store is used for global and user settings.
 *
 * with:
 * - definition (Name)
 * - defaults - defaults of this store (defaultName)
 * - the store itself (useName)
 *
 */

import { defineStore } from 'pinia'


enum Theme {
    System = 'system',
    Light = 'light',
    Dark = 'dark',
}


interface Settings {

    /**
     * The number of events to load on a page in a room.
     */

    pagination: number,

    /**
     * What message types will be visible as a normal message
     */

    visibleEventTypes: string[],

    /**
     * UI theme: system|dark|light
     */

    theme : Theme,

}


const defaultSettings: Settings = {
    theme : Theme.Dark,
    pagination: 50,
    visibleEventTypes: ['m.room.message'],
}


const useSettings = defineStore('settings', {

    state: () => {
        return defaultSettings as Settings;
    },

    getters: {

        getPagination: (state: Settings) => state.pagination,

        getVisibleEventTypes: (state: Settings) => state.visibleEventTypes,

        getTheme: (state:Settings) => {
            if ( state.theme != Theme.System ) {
                return state.theme;
            }
            if ( window.matchMedia('(prefers-color-scheme: dark)').matches)  {
                return Theme.Dark;
            }
            return Theme.Light;
        },

        isVisibleEventType : (state ) => (type:string) => {
            return state.visibleEventTypes.includes(type);
        }

    },

    actions: {

        setPagination(newPagination: number) {
            this.pagination = newPagination;
        },

        setTheme(newTheme:Theme) {
            this.theme = newTheme;
        }

    },

})

export { Theme, Settings, defaultSettings, useSettings }
