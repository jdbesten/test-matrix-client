import { ref } from 'vue';

const usedEvents = ['update','changed','cancel'];

const useFormInputEvents = (emit:any) => {

    const value = ref('');

    const changed = () => {
        emit('changed', value.value);
    }

    const submit = () => {
        if (value.value !== '') {
            emit('update', value.value);
        }
        value.value = '';
    }

    const cancel = () => {
        value.value = '';
        emit('cancel');
    }

    return { value, changed,submit,cancel }
}


export { useFormInputEvents, usedEvents };

