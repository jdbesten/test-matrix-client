import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { createRouter, createWebHashHistory } from 'vue-router';
import App from './App.vue'
import './registerServiceWorker'
import './assets/tailwind.css'

import { PubHubs } from './pubhubs';
import {routes} from './routes';

const router = createRouter({
    history: createWebHashHistory(),
    routes: routes,
});
const pinia = createPinia()
const app = createApp(App);

app.use(router);
app.use(pinia);
app.provide( 'pubhubs', new PubHubs() );
app.mount('#app');
